##################################################################################################################

# Installation & Configuration Windows Client
# Mael Coutoux
# Créé le :         22/03/2022
# Dernière modif :  19/09/2022

#################################################################################################################
############ Fonctions ############

# Verification configuration reseau
function CheckNetConf {
    Write-Host `n"Configuration reseau IPv4 actuelle : " -ForegroundColor Green `n
    #$IP_A = (Get-NetIPConfiguration | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*" }).IPv4Address.IPAddress
    $IP_A = (Get-NetIPaddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*"}).IPAddress
    $mask = (Get-NetIPaddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*"}).PrefixLength
    Write-Host "Adresse IPv4 : $IP_A /$mask"
    $IP_Gateway = (Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object {$_.IPEnabled}).DefaultIPGateway
    Write-Host "Adresse Gateway : $IP_Gateway" `n
}


# Configuration statique IP - Mask - Gateway
function ConfigurationIP {
    do {
        $netCard = (Get-NetIPConfiguration | Where-Object InterfaceAlias -like Ethernet*).InterfaceAlias
        Remove-NetIPAddress -InterfaceAlias $Netcard -SuffixOrigin Manual -ErrorAction SilentlyContinue -Confirm:$false
        $AdresseIpServeur = Read-Host "Nouvelle IPv4 ? "
        $mask = Read-Host "Longueur du masque ? 16,24... "
        $IP_Gateway = (Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object {$_.IPEnabled}).DefaultIPGateway
        Write-Host `n"Adresse Gateway : $IP_Gateway" `n
        $ChangeIPGateway = Read-Host "Modifier la gateway ?  y/n "
        if ($ChangeIPGateway -eq "y") {
            $NewIPGateway = Read-Host "IP de la gateway ? "
            # Suppression de la gateway existante
            Remove-NetRoute -DestinationPrefix 0.0.0.0/0 -Confirm:$false -Erroraction Ignore
            New-NetIPAddress -InterfaceAlias $netCard -IPAddress $AdresseIpServeur -PrefixLength $mask -DefaultGateway $NewIPGateway | Out-Null
        } else {
            New-NetIPAddress -InterfaceAlias $netCard -IPAddress $AdresseIpServeur -PrefixLength $mask | Out-Null
        }
        # Verification configuration reseau
        CheckNetConf
        $verifConf2 = Read-Host "Modifier de nouveau la configuration reseau ? y/n "
    } while ($verifConf2 -eq "y")
}


# Configuration reseau via DHCP
function ConfigurationDHCP {
    do {
        $netCard = (Get-NetIPConfiguration).InterfaceAlias
        Set-NetIPInterface -InterfaceAlias $netCard -Dhcp Enabled
        Restart-NetAdapter -InterfaceAlias $netCard
        CheckNetConf
        $verifConf3 = Read-Host "Modifier de nouveau la configuration reseau ? y/n "
    } while ($verifConf3 -eq "y")
}


# Configuration DNS Client
function ConfDnsClient {
    $netCard = (Get-NetIPConfiguration).InterfaceAlias
    $IP_Dns = (Get-DnsClientServerAddress -InterfaceAlias $netCard -AddressFamily IPv4).ServerAddresses
    Write-Host "Adresse(s) serveur(s) DNS : $IP_Dns" -ForegroundColor Green `n
    $changeDNS = Read-Host "Modifier les IP des serveurs DNS ? y/n "
    if ($changeDNS -eq "y") {
        do {
            $IPFirstDns = Read-Host "IP du serveur DNS ? "
            Set-DnsClientServerAddress -InterfaceAlias $netCard -ServerAddresses $IPFirstDns
            $twoDNS = Read-Host "Configuration d'un second serveur DNS ? y/n "
            if ($twoDNS -eq "y") {
                $IPSecondDns = Read-Host "IP du second server DNS ? "
                Set-DnsClientServerAddress -InterfaceAlias $netCard -ServerAddresses ("$IPFirstDns","$IPSecondDns")
            }
            Clear-DnsClientCache
            # Verification configuration
            $IP_Dns = (Get-DnsClientServerAddress -InterfaceAlias $netCard -AddressFamily IPv4).ServerAddresses
            Write-Host "Adresse(s) serveur(s) DNS configures : $IP_Dns" -ForegroundColor Green `n
            $verifConfDNS = Read-Host "Modifier de nouveau la configuration DNS ? y/n "
        } while ($verifConfDNS -eq "y")
    }
}


# Configuration reseau
function ConfNetwork {
    Write-Host `n"-- Configuration Reseau --" -ForegroundColor Green `n
    CheckNetConf
    $verifConf1 = Read-Host "Modifier la configuration reseau ? y/n "
    if ($verifconf1 -eq "y") {
        $EditionWindows = (Get-WindowsEdition -online).Edition
        if ($EditionWindows -notlike "*serv*") {
            $ifDHCP = Read-Host "Configurer en DHCP ? y/n "
            if ($ifDHCP -eq "y") {
                # Config DHCP si poste client et si souhaite
                ConfigurationDHCP
            } else { 
                # Config statique
                ConfigurationIP 
            }
        } else { 
            # Config statique
            ConfigurationIP 
        }
    }
}


# Integration a un domaine AD
function IntegrateDomain {
    Write-Host `n"-- Integration au Domaine AD --" -ForegroundColor Green `n
    $nameDomain = Read-Host "Quel est le nom de domaine ? "
    Add-Computer -DomainName $nameDomain -Credential Administrateur@$nameDomain
    Write-Host "Redemarrage obligatoire !" -ForegroundColor Red `n
    $RestartNow = Read-Host "Redemarrez votre PC maintenant ? y/n "
    if ($RestartNow -eq "y") {
        Restart-Computer -Confirm $true
    }
}


# Fonction verification + integration a un domaine AD
function VerifDomain {
    $verifDomain = (Get-WmiObject -Class Win32_ComputerSystem).PartOfDomain
    if ($verifDomain -eq $false) {
        $choice = read-host "Integration a un domaine ? y/n "
        if ($choice -eq "y") {
            ConfDnsClient
            IntegrateDomain
        }
    } else {
        $domain = (Get-ADDomain).DNSRoot
        Write-Host `n"Ce poste est deja integre au domaine $domain" `n
    }
}


# Affichage des RSAT installes sur un client
function WindowsRSATInstalled {
    Write-Host `n"RSAT deja installes sur ce poste : " -ForegroundColor Green
    Get-WindowsCapability -Name rsat* -online | Select-Object -Property DisplayName, State | Format-Table
}


# Disable UAC
function No_UserAccessControl {
    $desactivateUAC = Read-Host "Desactiver l'UAC ? y/n "
    if ($desactivateUAC -eq "y") {
        Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "ConsentPromptBehaviorAdmin" -Value 00000000 -Force
        Write-Host "User Access Control (UAC) has been disabled." -ForegroundColor Green
    }
}


#################################################################################################################
############ Script ############

Write-Host `n"######### Script de configuration de base de Windows #########" -ForegroundColor Red `n
# Désactivation de l'UAC
No_UserAccessControl

# Configuration reseau
ConfNetwork

# Configuration Client + RSAT
$EditionWindows = (Get-WindowsEdition -online).Edition
if ($EditionWindows -notlike '*serv*') {
    # Modifier le nom du poste 
    Write-Host `n"Le nom du PC est"$(hostname) `n -ForegroundColor Green
    $changeName = Read-Host "Changement de nom ? y/n "
    if ($changeName -eq "y") {
        $namePC = Read-Host "Hostname du PC ? "
        Rename-Computer -NewName $namePC
    }

    # Desactiver Par-feu
    $NoFirewall = Read-Host "Desactiver le par-feu sur les reseaux Domaine et Prive ? y/n "
    if ($NoFirewall -eq "y") {
        Set-NetFirewallProfile -Profile Domain,Private -Enabled False
    }

    # Creer un compte admin local
    $NewAdminLocal = Read-Host "Creer un nouveau compte admin local ? y/n "
    if ($NewAdminLocal -eq "y") {
        $Username = Read-Host "Username "
        $Password = Read-Host "Password " -AsSecureString
        New-LocalUser -Name $Username -Password $Password
        Add-LocalGroupMember -Group "Administrateurs" -Member $Username
    }
    # Installation des applications
    # Install Chocolatey
    # Set-ExecutionPolicy Bypass -Scope Process -Force 
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
    # Install Terminal Windows (This package requires at least Windows 10 version 1903/OS build 18362)
    Write-Host `n"Installation de Windows Terminal" -ForegroundColor Green `n
    choco install microsoft-windows-terminal /yes
    # Install PowerShell v7
    Write-Host `n"Installation de Powershell Core v7" -ForegroundColor Green `n
    choco install powershell-core /yes

    $install_Apps = Read-Host "Installer des applications : VSCode, Firefox, Acrobat Reader, MobaXterm, Greenshot, 7-Zip ? y/n "
    if ($install_Apps -eq "y") {
        $install_VScode = Read-Host "Installer VSCode ? y/n "
        $install_Firefox = Read-Host "Installer Firefox ? y/n "
        $install_Acrobat = Read-Host "Installer Acrobat Reader ? y/n "
        $install_Mobaxterm = Read-Host "Installer MobaXterm ? y/n "
        $install_Greenshot = Read-Host "Installer GreenShot ? y/n "
        $install_Zip = Read-Host "Installer 7-Zip ? y/n "
        
        if ($install_VScode -eq "y") {
            choco install vscode.install /yes
        }
        if ($install_Firefox -eq "y") {
            choco install firefox /yes
        }
        if ($install_Acrobat -eq "y") {
            choco install adobereader /yes
        }
        if ($install_Mobaxterm -eq "y") {
            choco install mobaxterm /yes
        }
        if ($install_Greenshot -eq "y") {
            choco install greenshot /yes
        }
        if ($install_Zip -eq "y") {
            choco install 7zip.install /yes
        }
    }

    # Recherche de paquets Chocolatey
    $Search_Paquet_Choco = Read-Host "Rechercher un paquet à installer ? y/n "
    if ($Search_Paquet_Choco -eq "y") {
        do {
            do {
                $Paquet_Choco = Read-Host "Recherche du nom du paquet à installer "
                choco search *$Paquet_Choco*
                $Paquet_Install = Read-Host "Saisir le nom du paquet à intaller, faire une nouvelle recherche (r) ou quitter (q) "
            } while ($Paquet_Install -eq "r")
            if ($Paquet_Install -eq "q") {
                $New_Install = "n"
            } else {
                choco install $Paquet_Install /yes
                $New_Install = Read-Host "Rechercher un autre paquet Chocolatey ? y/n "
            }
        } while ($New_Install -eq "y")
    }

    # Supprimer Alias bureau
    $User = ((Get-WMIObject -ClassName Win32_ComputerSystem).Username).Split('\')[1]
    Remove-Item -Path C:\Users\$User\Desktop\*.lnk
    Remove-Item -Path C:\Users\Public\Desktop\*.lnk

    # Configuration Windows Explorer for current user
    # afficher les extensions, les fichiers proteges, afficher chemin complet
    $key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
    Set-ItemProperty $key HideFileExt 0
    Set-ItemProperty $key ShowSuperHidden 1
    Set-ItemProperty $key ShowFullPath 1
    Set-ItemProperty $key LaunchTo 1
    Stop-Process -processname explorer
    $User = $env:UserName
    $ShellObj = New-Object -ComObject shell.application
    $SID = (Get-LocalUser -Name $env:UserName).sid.value
    # Supprimer tous les Quick Access
    ($ShellObj.Namespace("shell:::{679F85CB-0220-4080-B29B-5540CC05AAB6}").Items() | Where-Object { $_.Path -like 'C:\*' }).InvokeVerb("unpinfromhome")
    # Ajout Repertoire 'Bureau'
    $DesktopPath = 'C:\Users\'+($User)+'\Desktop'
    $ShellObj.Namespace($DesktopPath).Self.InvokeVerb("pintohome")
    # Ajout Repertoire 'Telechargements'
    $DownloadPath = 'C:\Users\'+($User)+'\Downloads'
    $ShellObj.Namespace($DownloadPath).Self.InvokeVerb("pintohome")
    # Corbeille pour l'utilisateur connecte
	$BinPath = 'C:\$Recycle.Bin\'+($SID)
	$ShellObj.Namespace($BinPath).Self.InvokeVerb("pintohome")

    # Installation des RSAT
    $install_RSAT = Read-host "Besoin d'installer des RSAT ? y/n "
    if ($install_RSAT -eq "y") {
        # Liste des RSAT installes
        WindowsRSATInstalled
        $install_RSAT_Gest_Ser = Read-Host "Installer les RSAT Gestionnaire de serveur ? y/n "
        $install_RSAT_ADDS = Read-Host "Installer les RSAT ADDS ? y/n "
        $install_RSAT_DNS = Read-Host "Installer les RSAT DNS ? y/n "
        $install_RSAT_DHCP = Read-Host "Installer les RSAT DHCP ? y/n "
        $install_RSAT_GPO = Read-Host "Installer les RSAT GPO ? y/n "
        $install_RSAT_ADCS = Read-Host "Installer les RSAT ADCS ? y/n "
        $install_RSAT_RDS = Read-Host "Installer les RSAT RDS ? y/n "
        $install_RSAT_IPAM = Read-Host "Installer les RSAT IPAM ? y/n "

        # Installation des RSAT Gestionnaire de serveur
        if ($install_RSAT_Gest_Ser -eq "y") {
            Add-WindowsCapability -“Online -“Name "Rsat.ServerManager.Tools~~~~0.0.1.0"
        }
        # Installation des RSAT ADDS
        if ($install_RSAT_ADDS -eq "y") {
            Add-WindowsCapability -“Online -“Name "Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0"
            Import-Module ActiveDirectory -PassThru
        }
        # Installation des RSAT DNS
        if ($install_RSAT_DNS -eq "y") {
            Add-WindowsCapability -“Online -“Name "Rsat.Dns.Tools~~~~0.0.1.0"
        }
        # Installation des RSAT DHCP
        if ($install_RSAT_DHCP -eq "y") {
            Add-WindowsCapability -“Online -“Name "Rsat.DHCP.Tools~~~~0.0.1.0"
        }
        # Installation des RSAT GPO
        if ($install_RSAT_GPO -eq "y") {
            Add-WindowsCapability -“Online -“Name "Rsat.GroupPolicy.Management.Tools~~~~0.0.1.0"
        }
        # Installation des RSAT ADCS
        if ($install_RSAT_ADCS -eq "y") {
            Add-WindowsCapability -“Online -“Name "Rsat.CertificateServices.Tools~~~~0.0.1.0"
        }
        # Installation des RSAT RDS
        if ($install_RSAT_RDS -eq "y") {
            Add-WindowsCapability -“Online -“Name "Rsat.RemoteDesktop.Services.Tools~~~~0.0.1.0"
        }
        # Installation des RSAT IPAM
        if ($install_RSAT_IPAM -eq "y") {
            Add-WindowsCapability -“Online -“Name "Rsat.IPAM.Client.Tools~~~~0.0.1.0"
        }
        # Liste des RSAT installes
        WindowsRSATInstalled
    }
}

# Verification si integre a un domaine, integration si besoin
VerifDomain




