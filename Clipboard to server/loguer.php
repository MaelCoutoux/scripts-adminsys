// Fichier à placer sur le serveur web avec un fichier data_receive.txt pour récupérer le contenu


<?php
if (isset($_GET['data']) && !empty($_GET['data'])) {
    // Vérifier si le paramètre 'data' est présent dans l'URL et n'est pas vide

    // Ouvrir le fichier en mode 'a+' (ajout en fin de fichier avec création s'il n'existe pas)
    $logfile = fopen('data_receive.txt', 'a+');

    // Écrire les données dans le fichier, suivi d'un saut de ligne pour séparer les entrées
    fwrite($logfile, $_GET['data'] . "\r\n");

    // Fermer le fichier
    fclose($logfile);

    // Afficher un message de succès
    echo "Données enregistrées avec succès.";
} else {
    // Afficher un message si le paramètre 'data' est vide ou inexistant
    echo "Data vide";
}
?>


// -----------------
// Autre version du loguer

<?php
if (isset($_GET['data']) && !empty($_GET['data'])) {
    $logfile = fopen('data_receive.txt', 'a+');

    if ($logfile) {
        fwrite($logfile, $_GET['data'] . "\r\n");
        fclose($logfile);
        echo "Données enregistrées avec succès.";
    } else {
        echo "Erreur lors de l'ouverture du fichier.";
    }
} else {
    echo "Paramètre 'data' manquant ou vide.";
}
?>

