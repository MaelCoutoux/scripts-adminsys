#!/bin/bash
#========================================================
#	FILE : get-process.sh	TP4
#	DESCRIPTION : Processus en cours pour un user
#	AUTHOR : Mael Coutoux
#	CREATED : 19/04/2022
#========================================================

#Test nombre parametres du script
if [[ $# -gt 1 ]] ; then
		echo -e "\n\033[1;31mUtilisation du script : nom_cript.sh [nom_process]\033[0m\n"
		exit 3
fi

# Si pas de paramètre, demande au user, si rien --> user courant
[[ -z $1 ]] && read -p "Quel est le critère de recherche ? " crit trash
if [[ -n $crit ]]
	then 
	critere=$crit
	else 
	critere=$(whoami)
fi

# Si un paramètre existe
[[ -n $1 ]] && critere=$1

clear
echo "--------------------------------------"
echo -e "Liste des processus contenant \"\033[1;32m$critere\033[0m\""
echo "--------------------------------------"
ps aux | head -1
ps aux | grep $critere
echo "--------------------------------------"
date=$(date +%r)
echo "$date- Fin de traitement"