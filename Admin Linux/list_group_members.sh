#!/bin/bash
#========================================================
#	FILE : TP11-3.sh
#	DEPENDENCE : 
#	DESCRIPTION : Afficher la liste des membres d'un groupe
#	AUTHOR : Mael C
#	CREATED : 22/04/2022
#========================================================
#clear
# Variables pour les couleurs
# Copyright Antoine Perol
Cinit="\033[0m"
Cred="\033[1;31m"
CGreen="\033[1;32m"
# Fin copyrignt
essai=0

# Vérification si un paramètre est passé au script
if [[ -z $1 ]]
    then
        echo "" ; read -p "Quel groupe ? >> " group_search trash
    else
        group_search=$1
fi

# Boucle tant que essai est <= 3
while [[ $essai -lt 3 ]]
    do
        # Vérification si le groupe existe
        group=$(cat /etc/group | grep "^$group_search:" | awk -F ":" '{print $1}')
        if [[ -z $group ]]
            then  # le groupe n'existe pas, affichage de la liste des groupes, nouvelle demande
                ((essai++))
                echo -e "$Cred\nCe groupe n'existe pas !$Cinit"
                echo -e "Voici la liste des groupes créés ayant 1 ou plusieurs menbres : \n"
                list_group=$(cat /etc/group | sed '1,63d' | grep -E ".*[:].*[:].*[:][a-z]+" | awk -F ":" '{print $1}')
                echo "$list_group"
                echo "" ; read -p "Quel groupe ? >> " group_search trash

            else  # Affichages des membres du groupe
                list_members=$(cat /etc/group | grep "$group:" | awk -F ":" '{print $4}')
                echo -e "\nLes membres du groupe $CGreen$group$Cinit sont : \n"
                echo -e "$CGreen$list_members$Cinit\n"
                exit 0
        fi
    done



