#!/bin/bash
#========================================================
#	FILE : tp8_script.sh	
#	DESCRIPTION : Affichage des infos des users
#	AUTHOR : Mael C
#	CREATED : 25/02/2021
#	REVISION :
#========================================================
#
# Extration des infos des comptes users avec uid a 4 chiffres | Bouclage avec while read
cat /etc/passwd | sed -e 's/::/:x:/g' -e 's/:/\ /g' | grep -E "^\w+\sx\s[0-9]{4}\s[0-9]{4}\s" | while read user trash uid gid desc rep shell ; do
echo -e "\n----------------------------------"
echo -e "Identifiant : \t \033[1;32m $user \033[0m"
echo -e "-  -  -  -  -  -  -  -  -  -  -  -"
echo -e "UID : $uid \t GID : $gid"
	if [[ $desc != x ]] ; then
echo -e "Description : \t $desc" ; fi
echo -e "Rep. personnel : $rep"
echo -e "Shell : \t $shell"
done
