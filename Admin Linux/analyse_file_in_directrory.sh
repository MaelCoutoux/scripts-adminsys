#!/bin/bash
#========================================================
#	FILE : tp7_script.sh	
#	DESCRIPTION : analyse des extensions de fichiers dans repertoire du parametre 1
#	AUTHOR : Mael C
#	CREATED : 24/02/2021
#	REVISION :
#========================================================
#
#Test nombre parametres du script
if [[ $# != 1 ]] 
	then
		echo -e "\n\033[1;31mCe script doit prendre uniquement 1 seul parametre : indiquer le chemin d'un repertoire\033[0m\n"
		exit 2
fi

#Test de la validite du repertoire
cd $1 2>/dev/null
if [[ $? = 1 ]]
	then 
		echo -e "\n\033[1;31mCe dossier n'existe pas\033[0m\n"
		exit 1
fi

#Affichage du nombre de fichiers dans le repertoire passe en parametre
nb_fic_all=$(ls -l $1 | grep -E "^-.*" | wc -l)
echo -e "\nNombre total de fichiers dans \033[1m$1 : $nb_fic_all \n\033[0m"

#Recuperation des extensions du fichier
fic_ext=$(cat extension.txt 2>/dev/null)

#Boucle for et test si fichiers trouves pour chaque extension presente
for each_ext in $fic_ext ; do

	nb_fic_ext=$(ls $1 | grep $each_ext | wc -l)
	pourcentage=$(expr \( $nb_fic_ext \* 100 \) / $nb_fic_all)

	if [[ $nb_fic_ext != 0 ]]
		then
			echo "------------"
			echo -e "   \033[1m$each_ext\033[0m"
			echo "------------"
			echo "-> $nb_fic_ext fichiers"
			echo -e "-> $pourcentage % des fichiers du repertoire\n"
	fi
done	
