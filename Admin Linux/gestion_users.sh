#!/bin/bash
#========================================================
#	FILE : tp5_script.sh	
#	DESCRIPTION : Gestion des users
#	AUTHOR : Mael C
#	CREATED : 24/02/2021
#	REVISION :
#========================================================
#
#Variables
essai=0
essai_pass=0
br=`echo -e "\n"`

echo $br
#Fonction de sortie
sortie () {
	echo $br
	echo "Bonne journee, bisou"
	echo $br
	exit 0	
}

#Array des utilisateurs connus
declare -a users
users=(rene sophie camille baptiste matthieu alix jean paul claire) 
#echo ${users[3]}

read -p "Saisir l'identifiant utilisateur souhaite : " user
echo $br
echo "GESTION DES UTILISATEURS : $user"
echo "------------------------"
echo "C - Creer le compte utilisateur"
echo "M - Modifier le mot de passe de l'utilisateur"
echo "S - Supprimer le compte utilisateur"
echo "V - Verifier si le compte utilisateur existe"
echo $br
echo "Q - Quitter"
echo $br
#Boucle tant que la saisie n'est pas correcte, avec 3 essais max
while [[ $letter != [CcMmSsVvQq] ]]
	do
		read -p "Votre choix : " letter
		((essai++))
		case $letter in
			C|c)
				echo "Creation du compte utilisateur..."
				sleep 2
				if [[ ${users[*]} =~ $user ]]
					then
						echo -e "\n\033[1;32mLe compte existe deja !\033[0m"
					else
						echo -e "\n\033[1;32mLe compte a bien ete cree !\033[0m"
				fi
				sortie
				;;
			M|m)
				echo "Modificattion du mot de passe"
				while [[ $pass != ?????? ]]
					do
						read -p "Saisir le nouveau not de passe : " pass
						if [[ $pass != ?????? ]]
							then
								echo -e "Le mot de passe doit avoir \033[1;31m6 caracteres\033[0m"
						fi
						((essai_pass++))
						if [[ $essai_pass -gt 2 ]]
							then
								echo -e "\n\033[1;31mMot de passe non modifie\033[0m"
								exit 3
						fi
					done
				sleep 2
				echo -e "\n\033[1;32mLe mot de passe a bien ete modifie !\033[0m"
				sortie
				;;
			S|s)
				echo "Suppression du compte utilisateur..."
				sleep 2
				echo -e "\n\033[1;32mLe compte a bien ete supprime\033[0m"
				sortie
				;;
			V|v)
				echo -e "\nVerification de l'existance du compte..."
				sleep 2
				if [[ ${users[*]} =~ $user ]]
					then
						echo -e "\n\033[1;32mLe compte existe deja !\033[0m"
					else 
						echo -e "\n\033[1;31mLe compte n'existe pas :-(\033[0m" 
				fi
				sortie
				;;
			Q|q)
				echo -e "\nMerci !"
				sortie
				;;
			*)
				if [[ $essai -gt 3 ]]
					then
						echo "Allez, au revoir !"
						exit 2
					else
						echo "Mauvaise touche ! Essaie encore une fois"
				fi
				;;
		esac
	done
