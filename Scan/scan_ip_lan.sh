#!/bin/bash

get_hostname() {
    hostname=$(host $1 | awk '/name pointer/ {print $5}')
    if [ -z "$hostname" ]; then
        hostname="N/A"
    fi
    echo "$hostname"
}

scan_local_network() {
    ips=()
    hostname=$(hostname)
    local_ips=$(host $hostname | awk '/has address/ {print $4}')
    for ip in $local_ips; do
        hostname=$(get_hostname $ip)
        ips+=("$ip $hostname")
    done
    echo "${ips[@]}"
}

scan_specific_network() {
    ips=()
    subnet=$(echo $1 | cut -d '/' -f 1)
    subnet=$(echo $subnet | cut -d '.' -f 1-3)

    for i in $(seq 1 255); do
        ip="$subnet.$i"
        hostname=$(get_hostname $ip)
        ips+=("$ip $hostname")
    done
    echo "${ips[@]}"
}

print_table() {
    echo -e "IP Address\tHostname"
    echo "-------------------------------------"
    for item in "${data[@]}"; do
        ip=$(echo $item | awk '{print $1}')
        hostname=$(echo $item | awk '{print $2}')
        echo -e "$ip\t$hostname"
    done
}

# Demande à l'utilisateur s'il souhaite scanner le LAN ou un réseau spécifique
read -p "Voulez-vous scanner le LAN local (L) ou un réseau spécifique (S) ? " option

if [[ $option == [Ll] ]]; then
    data=$(scan_local_network)
elif [[ $option == [Ss] ]]; then
    read -p "Entrez le réseau à scanner (par exemple, 192.168.0.0/24) : " network
    data=$(scan_specific_network $network)
else
    echo "Option invalide."
    exit 1
fi

IFS=$'\n' data=($data)

# Affichage des résultats sous forme de tableau si des données ont été récupérées
if [ ${#data[@]} -gt 0 ]; then
    print_table
fi
