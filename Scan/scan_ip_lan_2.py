#!/usr/bin/env python

import socket
import subprocess

def get_hostname(ip):
    try:
        hostname = socket.gethostbyaddr(ip)[0]
        return hostname
    except socket.herror:
        return "N/A"

def get_mac_address(ip):
    try:
        output = subprocess.check_output(["arp", "-n", ip])
        mac_address = output.decode().split()[3]
        return mac_address
    except subprocess.CalledProcessError:
        return "N/A"

def ping(ip):
    try:
        output = subprocess.check_output(["ping", "-c", "1", "-W", "1", ip])
        return True
    except subprocess.CalledProcessError:
        return False

def scan_local_network():
    ips = []
    hostname = socket.gethostname()
    local_ips = socket.gethostbyname_ex(hostname)[-1]
    for ip in local_ips:
        hostname = get_hostname(ip)
        mac_address = get_mac_address(ip)
        is_reachable = ping(ip)
        result = (ip, hostname, mac_address, is_reachable)
        ips.append(result)
    return ips

def scan_specific_network(network):
    ips = []
    subnet = network.split('.')
    subnet = subnet[0] + '.' + subnet[1] + '.' + subnet[2] + '.'

    for i in range(1, 256):
        ip = subnet + str(i)
        hostname = get_hostname(ip)
        mac_address = get_mac_address(ip)
        is_reachable = ping(ip)
        result = (ip, hostname, mac_address, is_reachable)
        ips.append(result)

    return ips

def print_table(data):
    print("IP Address\t\tHostname\t\tMAC Address\t\tPing Result")
    print("---------------------------------------------------------------------------")
    for ip, hostname, mac_address, is_reachable in data:
        reachable = "OK" if is_reachable else "Not reachable"
        print(f"{ip}\t\t{hostname}\t\t{mac_address}\t\t{reachable}")

# Demande à l'utilisateur s'il souhaite scanner le LAN ou un réseau spécifique
option = input("Voulez-vous scanner le LAN local (L) ou un réseau spécifique (S) ? ")

# Appel de la fonction appropriée en fonction de l'option choisie
if option.lower() == 'l':
    scan_data = scan_local_network()
elif option.lower() == 's':
    network = input("Entrez le réseau à scanner (par exemple, 192.168.0.0/24) : ")
    scan_data = scan_specific_network(network)
else:
    print("Option invalide.")
    exit()

# Affichage des résultats sous forme de tableau
print_table(scan_data)
