# ESD - Python pour pentest
# Scanner de ports
# 22/01/2024
# Maël Coutoux

#!/usr/bin/env python3

import sys
import socket
import time
# import pyfiglet
from socket import getservbyname


# ascii_banner = pyfiglet.figlet_format("PORT SCANNER")
# print(ascii_banner)

# IP à scanner
ip = str(input("IP hôte ? : "))

# Range de ports à scanner
port_start = int(input("1er port à scanner ? : "))
port_end = int(input("Dernier port à scanner ? : "))

# Variables
ports = range(port_start, port_end)
open_ports =[] 

LHOST = '127.0.0.1'
LPORT = 666

def scan_port(ip, port, result = 1): 
    try: 
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        sock.settimeout(0.001)
        test = sock.connect_ex((ip, port))
        if test == 0: 
            result = test
        sock.close() 
    except:
        pass 
    return result

def get_service(port):
    port = str(port)
    service = socket.getservbyname(port)
    return service

start = time.time()

for port in ports: 
    sys.stdout.flush() 
    response = scan_port(ip, port) 
    service = get_service(port)
    if response == 0: 
        open_ports.append(port) 
    
if open_ports: 
    print ("\nNombre de ports ouvert :", len(open_ports))
    print ("\nPorts ouverts sur le poste", ip, ": ") 
    print (*open_ports, service, sep=" - ")
else: 
    print ("Aucun port ouvert...")

# Affichage de la durée du scan
print("\n==> Durée du scan :", round(time.time() - start, 2), "secondes\n")

