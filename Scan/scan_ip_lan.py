#!/usr/bin/env python

import socket
import subprocess
import sys
import dns.resolver
import ipaddress

# Vérifier si le module dnspython est installé, sinon installer
try:
    dns.resolver.resolve('example.com', 'A')
except (ImportError, dns.resolver.NoAnswer):
    print("Le module 'dnspython' n'est pas installé. Tentative d'installation en cours...")

    try:
        subprocess.check_call([sys.executable, "-m", "pip3", "install", "dnspython"])
        import dns.resolver
    except subprocess.CalledProcessError:
        print("Erreur lors de l'installation du module 'dnspython'. Veuillez l'installer manuellement.")
        sys.exit(1)

# Fonction pour récupérer le nom d'hôte
def get_hostname(ip):
    try:
        return socket.gethostbyaddr(ip)[0]
    except socket.herror:
        return "N/A"

# Fonction pour récupérer l'adresse MAC
def get_mac_address(ip):
    try:
        output = subprocess.check_output(['arp', '-n', ip]).decode()
        mac = output.split()[3]
        return mac
    except subprocess.CalledProcessError:
        return "N/A"

# Fonction pour effectuer un ping
def ping(ip):
    try:
        subprocess.check_output(['ping', '-c', '1', '-W', '1', ip])
        return "OK"
    except subprocess.CalledProcessError:
        return "NOK"
    except KeyboardInterrupt:
        sys.exit(1)

# Fonction pour scanner le LAN local
def scan_local_network():
    local_ip = socket.gethostbyname(socket.gethostname())
    network = ipaddress.ip_network(local_ip + '/24', strict=False)
    data = []

    for ip in network.hosts():
        ip = str(ip)
        hostname = get_hostname(ip)
        mac_address = get_mac_address(ip)
        ping_status = ping(ip)

        data.append((ip, hostname, mac_address, ping_status))

    print_table(data)

# Fonction pour scanner un réseau spécifique
def scan_specific_network(network):
    network = ipaddress.ip_network(network, strict=False)
    data = []

    for ip in network.hosts():
        ip = str(ip)
        hostname = get_hostname(ip)
        mac_address = get_mac_address(ip)
        ping_status = ping(ip)

        data.append((ip, hostname, mac_address, ping_status))

    print_table(data)

# Fonction pour afficher les résultats sous forme de tableau
def print_table(data):
    print("+-----------------+------------------+-----------------+------------+")
    print("|      Adresse IP      |      Nom d'hôte      |      Adresse MAC      |   Ping   |")
    print("+-----------------+------------------+-----------------+------------+")

    for item in data:
        ip, hostname, mac_address, ping_status = item
        print(f"| {ip:<16} | {hostname:<18} | {mac_address:<19} | {ping_status:<8} |")

    print("+-----------------+------------------+-----------------+------------+")


# Demande à l'utilisateur s'il souhaite scanner le LAN ou un réseau spécifique
def main():
    print("1. Scanner le LAN local")
    print("2. Scanner un réseau spécifique")

    choice = input("Veuillez sélectionner une option (1 ou 2): ")

    if choice == '1':
        scan_local_network()
    elif choice == '2':
        network = input("Veuillez entrer le réseau à scanner (ex: 192.168.0.0/24): ")
        scan_specific_network(network)

# Exécute le programme principal
if __name__ == "__main__":
    main()