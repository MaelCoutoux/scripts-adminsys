# " La root est longue, mais la voie est libre "

[_root-me.org_](https://root-me.org)

[![maelcoutoux.fr](https://img.shields.io/badge/-maelcoutoux.fr-9cf)](https://maelcoutoux.fr) [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=flat&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/maelcoutoux/)

--------------

## Scripts pour scan réseau

- Scan IP
- Scan Ports

## Script clipboard to server : clipboard.py

- Script qui envoie le presse papier dans un fichier sur un serveur web (à utiliser avec loguer.php)

## Scripts pour configuration Windows 10 / 11 : Conf_Client.ps1

_**Configuration d'un client Windows 10 / 11**_

- Modification Hostname, Création compte admin local
- Configuration réseau
- Configuration de l'explorateur de fichiers
- Installation d'applications (via Chocolatey)
- Installation des RSAT
- Intégration à un domaine AD

> Le fichier _.bat_ permet de lancer le script du même nom en tant qu'administrateur

## Scripts pour configuration services réseaux sous Windows Server 2019 / 2022

- Configuration _Serveur ADDS_
- Configuration _Serveur DHCP_
- Gestion des OU / groupes / utilisateurs de l'AD

## Scripts admin 365 : office365_users/ps1

- Gestion des utilisateurs 365

## Scripts admin sous Debian

- Gestion des utilisateurs
- Analyse de fichiers
- Processus en cours pour un user

--------------

[![maelcoutoux.fr](https://img.shields.io/badge/-maelcoutoux.fr-9cf)](https://maelcoutoux.fr) [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=flat&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/maelcoutoux/)
