Install-Module msonline
# Connexion 
Connect-MsolService
# Account ID
Get-MsolAccountSku


# List des users
Get-MsolUser
Get-MsolUser | Select-Object displayname, UserPrincipalName, title, usagelocation, islicensed | Format-Table
# Add user
New-MsolUser -UserPrincipalName jacqueline@user@domain.onmicrosoft.com -UsageLocation FR -Title Secretaire -DisplayName Jacqueline -FirstName Jacque -LastName Line -Password ....
# Modif user
Set-MsolUser -UserPrincipalName ....  -Title .....
# Modif password
Set-MsolUserPassword -UserPrincipalName ... -NewPassword ...
# Suppression user et restauration
Remove-MsolUser -UserPrincipalName ....
Get-MsolUser -ReturnDeletedUsers
Restore-MsolUser -UserPrincipalName ....


# List des roles
Get-MsolRole | Select-Object name, description | Sort-Object name
# Ajout role Admin Global
Add-MsolRoleMember -RoleName "Company Administrator" -RoleMemberEmailAddress jacqueline@domain.onmicrosoft.com
# Ajout role Admin de licence
Add-MsolRoleMember -RoleName "License Administrator" -RoleMemberEmailAddress ....
# Ajout role Admin de password
Add-MsolRoleMember -RoleName "Password Administrator" -RoleMemberEmailAddress ....


# List des services et applications du plan 
(Get-MsolAccountSku).servicestatus
# Ajout Licence
Set-MsolUserLicense -UserPrincipalName user@domain.onmicrosoft.com -AddLicenses (Get-MsolAccountSku).accountskuid
# Aplication d'une licence sans certaines applications 
$low = New-MsolLicenseOptions -AccountSkuId "user@domain:ENTERPRISEPREMIUM" -DisabledPlans SWAY,BI_AZURE_P2,RMS_S_ENTERPRISE
Set-MsolUserLicense -UserPrincipalName user@domain.onmicrosoft.com -LicenseOptions $low
# Verif : List des applications par user
(Get-MsolUser -UserPrincipalName user@domain.onmicrosoft.com).licenses.servicestatus

# Import users via fichier CSV
Import-Csv -path C:\path\to\file\user.csv | ForEach-Object {
    New-MsolUser 
    -UserPrincipalName $_."Nom d'utilisateur" 
    -DisplayName $_."Nom complet" 
    -FirstName $_."Nom d'utilisateur" 
    -LastName $_.Nom 
    -Department $_.Service 
    -Title $_.fonction 
    -UsageLocation fr 
    -Password Pa55w.rd 
    -ForceChangePassword $true
}

# Ajout licences en masse 
Get-MsolUser -All -UnlicensedUsersOnly -UsageLocation fr | ForEach-Object {
    Set-MsolUserLicense -UserPrincipalName $_.UserPrincipalName -AddLicenses (Get-MsolAccountSku).accountskuid
}
