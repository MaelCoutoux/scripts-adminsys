##################################################################################################################

# Installation & Configuration Windows Server - DHCP
# Mael Coutoux
# Cree le :         22/03/2022
# Derniere modif :  19/09/2022

#################################################################################################################
############ Fonctions ############


# Verification configuration reseau
function CheckNetConf {
    Write-Host `n"Configuration reseau IPv4 actuelle : " -ForegroundColor Green `n
    #$IP_A = (Get-NetIPConfiguration | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*" }).IPv4Address.IPAddress
    $IP_A = (Get-NetIPaddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*"}).IPAddress
    $mask = (Get-NetIPaddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*"}).PrefixLength
    Write-Host "Adresse IPv4 : $IP_A /$mask"
    $IP_Gateway = (Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object {$_.IPEnabled}).DefaultIPGateway
    Write-Host "Adresse Gateway : $IP_Gateway" `n
}


# Configuration statique IP - Mask - Gateway
function ConfigurationIP {
    do {
        $netCard = (Get-NetIPConfiguration | Where-Object InterfaceAlias -like Ethernet*).InterfaceAlias
        Remove-NetIPAddress -InterfaceAlias $Netcard -SuffixOrigin Manual -ErrorAction SilentlyContinue -Confirm:$false
        $AdresseIpServeur = Read-Host "Nouvelle IPv4 ? "
        $mask = Read-Host "Longueur du masque ? 16,24... "
        $IP_Gateway = (Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object {$_.IPEnabled}).DefaultIPGateway
        Write-Host `n"Adresse Gateway : $IP_Gateway" `n
        $ChangeIPGateway = Read-Host "Modifier la gateway ?  y/n "
        if ($ChangeIPGateway -eq "y") {
            $NewIPGateway = Read-Host "IP de la gateway ? "
            # Suppression de la gateway existante
            Remove-NetRoute -DestinationPrefix 0.0.0.0/0 -Confirm:$false -Erroraction Ignore
            New-NetIPAddress -InterfaceAlias $netCard -IPAddress $AdresseIpServeur -PrefixLength $mask -DefaultGateway $NewIPGateway | Out-Null
        } else {
            New-NetIPAddress -InterfaceAlias $netCard -IPAddress $AdresseIpServeur -PrefixLength $mask | Out-Null
        }
        # Verification configuration reseau
        CheckNetConf
        $verifConf2 = Read-Host "Modifier de nouveau la configuration reseau ? y/n "
    } while ($verifConf2 -eq "y")
}


# Configuration DNS Client
function ConfDnsClient {
    $netCard = (Get-NetIPConfiguration).InterfaceAlias
    $IP_Dns = (Get-DnsClientServerAddress -InterfaceAlias $netCard -AddressFamily IPv4).ServerAddresses
    Write-Host "Adresse(s) serveur(s) DNS : $IP_Dns" -ForegroundColor Green `n
    $changeDNS = Read-Host "Modifier les IP des serveurs DNS ? y/n "
    if ($changeDNS -eq "y") {
        do {
            $IPFirstDns = Read-Host "IP du serveur DNS ? "
            Set-DnsClientServerAddress -InterfaceAlias $netCard -ServerAddresses $IPFirstDns
            $twoDNS = Read-Host "Configuration d'un second serveur DNS ? y/n "
            if ($twoDNS -eq "y") {
                $IPSecondDns = Read-Host "IP du second server DNS ? "
                Set-DnsClientServerAddress -InterfaceAlias $netCard -ServerAddresses ("$IPFirstDns","$IPSecondDns")
            }
            Clear-DnsClientCache
            # Verification configuration
            $IP_Dns = (Get-DnsClientServerAddress -InterfaceAlias $netCard -AddressFamily IPv4).ServerAddresses
            Write-Host "Adresse(s) serveur(s) DNS configures : $IP_Dns" -ForegroundColor Green `n
            $verifConfDNS = Read-Host "Modifier de nouveau la configuration DNS ? y/n "
        } while ($verifConfDNS -eq "y")
    }
}


# Configuration reseau
function ConfNetwork {
    Write-Host `n"-- Configuration Reseau --" -ForegroundColor Green `n
    CheckNetConf
    $verifConf1 = Read-Host "Modifier la configuration reseau ? y/n "
    if ($verifconf1 -eq "y") {        
        ConfigurationIP 
    }
}


# Integration a un domaine AD
function IntegrateDomain {
    Write-Host `n"-- Integration au Domaine AD --" -ForegroundColor Green `n
    $nameDomain = Read-Host "Quel est le nom de domaine ? "
    Add-Computer -DomainName $nameDomain -Credential Administrateur@$nameDomain
    Write-Host "Redemarrage obligatoire !" -ForegroundColor Red `n
    $RestartNow = Read-Host "Redemarrez votre PC maintenant ? y/n "
    if ($RestartNow -eq "y") {
        Restart-Computer -Confirm $true
    }
}


# Fonction verification + integration a un domaine AD
function VerifDomain {
    $verifDomain = (Get-WmiObject -Class Win32_ComputerSystem).PartOfDomain
    if ($verifDomain -eq $false) {
        $choice = read-host "Integration a un domaine ? y/n "
        if ($choice -eq "y") {
            ConfDnsClient
            IntegrateDomain
        }
    } else {
        $domain = (Get-ADDomain).DNSRoot
        Write-Host `n"Ce poste est deja integre au domaine $domain" `n
    }
}


#################################################################################################################
############ Script ############


# Configuration reseau
ConfNetwork


# Configuration DNS
ConfDnsClient


# Verification si poste integre à un domaine, integration si besoin
VerifDomain 


Write-Host `n`t"-- Installation et Configuration Serveur DHCP --" -ForegroundColor Red `n
# CD or not CD...
$FQDN = (Get-ADComputer -Identity $(hostname)).DNSHostName
$IsServerCD = (Get-ADDomain).Replicadirectoryservers
if ($IsServerCD -notcontains $FQDN) {  # Si le serveur n'est pas contrôleur de domaine :
    # Remommage du serveur
    Write-Host 'Le nom du serveur est'$(hostname)  `n
    $changeName = Read-Host  "Changement de nom ? y/n "
    if ($changeName -eq "y") {
        $nameServerDHCP = Read-Host "Hostname du serveur ? " 
        Rename-Computer -NewName $nameServerDHCP
    }
}


# Verification si rôle DHCP dejà installe + Installation
$NameServerCD = (Get-ADDomainController | Where-Object {$_.OperationMasterRoles -contains "SchemaMaster"}).name
$IpServerCD = (Get-ADDomainController | Where-Object {$_.OperationMasterRoles -contains "SchemaMaster"}).IPv4Address
$ServiceDHCP = (Get-WindowsFeature | Where-Object{$_.Name -eq "DHCP"}).InstallState
WindowsFeatureInstalled
if ($ServiceDHCP -ne $Installed) {
    $InstallDHCP = Read-Host "Installer le rôle DHCP ? y/n "
    if ($InstallDHCP -eq "y") {
        Install-WindowsFeature -Name DHCP -IncludeManagementTools
        Add-DhcpServerInDC -DnsName $NameServerCD -IPAddress $IpServerCD
        Set-ItemProperty -Path registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\ServerManager\Roles\12 -Name ConfigurationState -Value 2
        }
    }


# Configuration de l'etendue
Do {
    $ScopeName= Read-Host "Nom de l'etendue ? "
    $ScopeId = Read-Host "Adresse reseau de l'etendue ? "
    $SubnetMaskRange = Read-Host "Masque reseau de l'etendue ? "
    $FirstIPRange = Read-Host "Première IP de l'etendue ? "
    $LastIPRange = Read-Host "Dernière IP de l'etendue ? "
    $Jour = Read-Host "Duree du bail en jours ? "

    # Si exclusion à configurer
    $ExclusionRange = Read-Host "Plage d'IP à exclure ? y/n "
    if ($ExclusionRange -eq "y" ) {
        $FirstIPExcluded = Read-Host `t"Première IP de la plage de l'exclusion ? "
        $LastIPExcluded = Read-Host `t"Dernière IP de la plage de l'exclusion ? "
    }

    # Options à configurer
    $IPGateway = Read-Host "IP de la gateway ? "
    $IPDns = Read-Host "IP du server DNS ? "
    $SuffixDns = (Get-ADDomain).DNSRoot

    # Creation de l'etendue
    Add-DhcpServerv4Scope -Name $ScopeName -StartRange $FirstIPRange -EndRange $LastIPRange -SubnetMask $SubnetMaskRange
    Set-DhcpServerv4Scope -ScopeId $ScopeId -LeaseDuration ($Jour)+'.00:00:00'
    if ($ExclusionRange -eq "y" ) {
        Add-DHCPServerV4ExclusionRange -ScopeId $ScopeId -StartRange $FirstIPExcluded -EndRange $LastIPExcluded
    }

    # Configuration des options
    Set-DhcpServerv4OptionValue -OptionId 3 -Value $IPGateway -ScopeID $ScopeId       # Passerelle
    Set-DhcpServerv4OptionValue -OptionId 6 -Value $IPDns -ScopeID $ScopeId           # Serveur DNS
    Set-DhcpServerv4OptionValue -OptionId 15 -Value $SuffixDns -ScopeID $ScopeId      # Suffixe DNS

    # Activation de l'etendue
    Set-DhcpServerv4Scope -ScopeId $ScopeId -Name $ScopeName -State Active
    Restart-service dhcpserver

    # Configurer une nouvelle etendue ?
    $newRange = Read-Host "Configurer une nouvelle etendue ? y/n "
} while ($newRange -eq "y")


# Verification des configuration des etendues
Write-Host `n"-- Etendue(s) configuree(s) --" -ForegroundColor Green `n
Get-DhcpServer4Scope -ComputerName $(hostname)


# Export des configuration des etendues en html
Get-DhcpServer4Scope -ComputerName $(hostname) | ConvertTO-HTML | Out-File C:\Configuration_etendues.html
Invoke-Item 'C:\Configuration_etendues.html'


# Redemarrage
Write-Host "Redemarrage obligatoire !" -ForegroundColor Red `n
$RestartNow = Read-Host "Redemarrez votre PC maintenant ? y/n "
if ($RestartNow -eq "y") {
    Restart-Computer -Confirm $true

