##################################################################################################################

# Installation & Configuration Windows Server - ADDS
# Mael Coutoux
# Créé le :         29/03/2022
# Dernière modif :  20/09/2022

#################################################################################################################
############ Fonctions ############

# Ajouter nouveau domaine a foret existante
# https://docs.microsoft.com/en-us/powershell/module/addsdeployment/install-addsdomain?view=windowsserver2022-ps
function NewDomaintoForest {
    Write-Host `n"-- Ajouter nouveau domaine a foret existante --"  -ForegroundColor Green `n
    Write-Host `n"--  /!\  Redemarrage automatique après promotion du controleur de domaine  /!\ --"  -ForegroundColor Red `n
    $nameDomain = Read-Host "Entrez un nom de domaine ? nom.domain "
    $nameNetBIOS = Read-Host "Entrez un nom NetBIOS ? (EN MAJUSCULES)"
    Import-Module ADDSDeployment
    
}


# Ajouter un CD au domaine existant
# https://docs.microsoft.com/en-us/powershell/module/addsdeployment/install-addsdomaincontroller?view=windowsserver2022-ps
function AddCDtoDomain {
    Write-Host `n"-- Ajouter un controleur de domaine au domaine existant --"  -ForegroundColor Green `n
    Write-Host `n"--  /!\  Redemarrage automatique après promotion du controleur de domaine  /!\ -- "  -ForegroundColor Red `n
    $nameDomain = Read-Host "Entrez le nom de domaine existant ? nom.domain "
    $nameNetBIOS = Read-Host "Entrez un nom NetBIOS ? (EN MAJUSCULES) "
    Import-Module ADDSDeployment
    
}


# Ajouter une nouvelle forêt
function NewForest {
    Write-Host `n"-- Creation d'un domaine racine de forêt --"  -ForegroundColor Green `n
    Write-Host `n"--  /!\  Redemarrage automatique après promotion du controleur de domaine  /!\ --"  -ForegroundColor Red `n
    $nameDomain = Read-Host "Entrez un nom de domaine ? nom.domain "
    $nameNetBIOS = Read-Host "Entrez un nom NetBIOS ? (EN MAJUSCULES)"
    Import-Module ADDSDeployment
    Install-ADDSForest `
    -CreateDnsDelegation:$false `
    -DatabasePath "C:\Windows\NTDS" `
    -DomainMode "WinThreshold" `
    -DomainName "$nameDomain" `
    -DomainNetbiosName "$nameNetBIOS" `
    -ForestMode "WinThreshold" `
    -InstallDns:$true `
    -LogPath "C:\Windows\NTDS" `
    -NoRebootOnCompletion:$false `
    -SysvolPath "C:\Windows\SYSVOL" `
    -Force:$true
    # Redemarrage du serveur
}


function CheckNetConf {
    Write-Host `n"Configuration reseau IPv4 actuelle : " -ForegroundColor Green `n
    #$IP_A = (Get-NetIPConfiguration | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*" }).IPv4Address.IPAddress
    $IP_A = (Get-NetIPaddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*"}).IPAddress
    $mask = (Get-NetIPaddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -like "Ethernet*" -and $_.IPAddress -notlike "169*"}).PrefixLength
    Write-Host "Adresse IPv4 : $IP_A /$mask"
    $IP_Gateway = (Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object {$_.IPEnabled}).DefaultIPGateway
    Write-Host "Adresse Gateway : $IP_Gateway" `n
}


# Configuration statique IP - Mask - Gateway
function ConfigurationIP {
    do {
        $netCard = (Get-NetIPConfiguration | Where-Object InterfaceAlias -like Ethernet*).InterfaceAlias
        Remove-NetIPAddress -InterfaceAlias $Netcard -SuffixOrigin Manual -ErrorAction SilentlyContinue -Confirm:$false
        $AdresseIpServeur = Read-Host "Nouvelle IPv4 ? "
        $mask = Read-Host "Longueur du masque ? 16,24... "
        $IP_Gateway = (Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object {$_.IPEnabled}).DefaultIPGateway
        Write-Host `n"Adresse Gateway : $IP_Gateway" `n
        $ChangeIPGateway = Read-Host "Modifier la gateway ?  y/n "
        if ($ChangeIPGateway -eq "y") {
            $NewIPGateway = Read-Host "IP de la gateway ? "
            # Suppression de la gateway existante
            Remove-NetRoute -DestinationPrefix 0.0.0.0/0 -Confirm:$false -Erroraction Ignore
            New-NetIPAddress -InterfaceAlias $netCard -IPAddress $AdresseIpServeur -PrefixLength $mask -DefaultGateway $NewIPGateway | Out-Null
        } else {
            New-NetIPAddress -InterfaceAlias $netCard -IPAddress $AdresseIpServeur -PrefixLength $mask | Out-Null
        }
        # Verification configuration reseau
        CheckNetConf
        $verifConf2 = Read-Host "Modifier de nouveau la configuration reseau ? y/n "
    } while ($verifConf2 -eq "y")
}


# Configuration DNS Client
function ConfDnsClient {
    $netCard = (Get-NetIPConfiguration).InterfaceAlias
    $IP_Dns = (Get-DnsClientServerAddress -InterfaceAlias $netCard -AddressFamily IPv4).ServerAddresses
    Write-Host "Adresse(s) serveur(s) DNS : $IP_Dns" -ForegroundColor Green `n
    $changeDNS = Read-Host "Modifier les IP des serveurs DNS ? y/n "
    if ($changeDNS -eq "y") {
        do {
            $IPFirstDns = Read-Host "IP du serveur DNS ? "
            Set-DnsClientServerAddress -InterfaceAlias $netCard -ServerAddresses $IPFirstDns
            $twoDNS = Read-Host "Configuration d'un second serveur DNS ? y/n "
            if ($twoDNS -eq "y") {
                $IPSecondDns = Read-Host "IP du second server DNS ? "
                Set-DnsClientServerAddress -InterfaceAlias $netCard -ServerAddresses ("$IPFirstDns","$IPSecondDns")
            }
            Clear-DnsClientCache
            # Verification configuration
            $IP_Dns = (Get-DnsClientServerAddress -InterfaceAlias $netCard -AddressFamily IPv4).ServerAddresses
            Write-Host "Adresse(s) serveur(s) DNS configures : $IP_Dns" -ForegroundColor Green `n
            $verifConfDNS = Read-Host "Modifier de nouveau la configuration DNS ? y/n "
        } while ($verifConfDNS -eq "y")
    }
}


# Configuration reseau
function ConfNetwork {
    Write-Host `n"-- Configuration Reseau --" -ForegroundColor Green `n
    CheckNetConf
    $verifConf1 = Read-Host "Modifier la configuration reseau ? y/n "
    if ($verifconf1 -eq "y") {        
        ConfigurationIP 
    }
}


# Affichage des rôles & fonctionnalités installés sur un serveur
function WindowsFeatureInstalled {
    Write-Host `n"Rôles & Fonctionnalités déjà installés sur ce serveur : " -ForegroundColor Green
    Get-WindowsFeature | Where-Object {$_.InstallState -eq "Installed"} | Format-Table
}


#################################################################################################################
############ Script ############

Write-Host `n`t"-- Installation et Configuration Controleur de domaine AD, serveur DNS --" -ForegroundColor Red `n

# Configuration reseau
ConfNetwork


# Configuration DNS
ConfDnsClient


# Configuration NTP
Write-Host `n"-- Configuration du service NTP --" -ForegroundColor Green `n
Write-Host "Synchronisation avec le serveur fr.pool.ntp.org" -ForegroundColor Green `n
w32tm /config /manualpeerlist:fr.pool.ntp.org,0x8 /syncfromflags:manual /reliable:yes /update
w32tm /resync /rediscover
net stop w32time
net start w32time
Write-Host "Service NTP configure !" -ForegroundColor Green `n


# Verification si roles deja installes & Installation
Write-Host `n"-- Installation des roles ADDS + DNS --" -ForegroundColor Green `n
$serviceADDS = (Get-WindowsFeature | Where-Object{$_.Name -eq "AD-Domain-Services"}).InstallState
$serviceDNS = (Get-WindowsFeature | Where-Object{$_.Name -eq "DNS"}).InstallState
if ($serviceADDS -ne $Installed) {
    Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
    }
if ($serviceDNS -ne $Installed) {
    Install-WindowsFeature -Name DNS -IncludeManagementTools
    }
WindowsFeatureInstalled
Write-Host "Les roles ADDS & DNS sont installes" -ForegroundColor Green `n
Start-Sleep 5


# Promotion du Controleur de domaine
Write-Host `n"-- Promotion du controleur de domaine --" -ForegroundColor Green `n
do {
    Write-Host "Quel choix pour la promotion de ce controleur de domaine ?"
    #### En cours de developpememt ####
    # Write-Host `t"1 - Ajouter nouveau domaine a foret existante"
    # Write-Host `t"2 - Ajouter un controleur de domaine au domaine existant"
    Write-Host `t"3 - Creer une nouvelle forêt"
    Write-Host `t"4 - Quitter" `n
    $choicePromo = Read-Host "Choix 1, 2, 3 ou 4 "
    switch ($choicePromo) {
        #1 { NewDomaintoForest }
        #2 { AddCDtoDomain }
        3 { NewForest }
        3 { Exit }
        Default { 
            Write-Host `n`t"Essaie encore une fois !" `n
            $retour = "play_again"
        }
    }
} while ($retour -eq "play_again")


# Renommage du serveur si besoin
Write-Host 'Le nom du serveur est'$(hostname) `n
$changeName = Read-Host "Changement de nom ? ==> Redemarrage du serveur !  y/n "
if ($changeName -eq "y") {
    $nameServer = Read-Host "Hostname du serveur ? "
    Rename-Computer -NewName $nameServer -Restart
}

