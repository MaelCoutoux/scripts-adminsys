# Listes des Users actifs

Get-ADUser -filter * -Properties * | Where-Object enabled -eq $true | Select-Object Name,City,Department | sort City,Departement,Name -Descending | Format-Table 

# Listes des Postes et Users --> Noms et date dernier login

Get-ADComputer -filter * -Properties * | Where-Object enabled -eq $true | Select-Object name,lastlogondate | sort lastlogondate -Descending
Get-ADUser -filter * -Properties * | Where-Object enabled -eq $true | Select-Object name,lastlogondate | sort lastlogondate -Descending


# Liste html des scripts d'ouverture de session sur les comptes actifs

Get-ADUser -filter * -Properties * | Where-Object enabled -eq $true | Select-Object Name,City,ScriptPath | sort scriptpath,city,name -Descending | ConvertTo-Html | Out-File C:\FichierBAT.html


# Modification UPN 

$LocalUsers = Get-ADUser -Filter "UserPrincipalName -like '*domaine.local'" -SearchBase "OU=Utilisateurs,OU=*****,DC=domaine,DC=local" -Properties userPrincipalName -ResultSetSize $null
$LocalUsers | ForEach-Object {$newUpn = $_.UserPrincipalName.Replace("@domaine.local","@domaine.fr"); $_ | Set-ADUser -UserPrincipalName $newUpn} 


# Modification d'un attribut (ex. proxyAddresses)

$users = Get-ADUser -Filter "UserPrincipalName -like '*@domaine.fr'" -Properties *
foreach ($user in $users) {
    $name = $user.samaccountname
    Set-ADUser $name -Replace @{proxyAddresses="SMTP:$name@domaine.fr"}
} 
Get-ADUser -Filter UserPrincipalName -like '*@domaine.fr' -SearchBase "OU=Utilisateurs,OU=****,DC=domaine,DC=local" -Properties userPrincipalName,proxyAddresses -ResultSetSize $null | Select-Object Name,UserPrincipalName,proxyAddresses 





