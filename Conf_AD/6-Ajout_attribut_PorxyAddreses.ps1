
# Script pour ajouter l'attribut AD ProxyAddresses

$users = Get-ADUser -Filter "UserPrincipalName -like '*@domain.fr'" -Properties *

foreach ($user in $users) {
    $name = $user.samaccountname
    Set-ADUser $name -Replace @{proxyAddresses="SMTP:$name@domain.fr"}
}


