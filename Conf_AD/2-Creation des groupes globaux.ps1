﻿
# Variables
$Infosdomaine = Get-ADDomain
$domainname = $infosdomaine.DistinguishedName
# Creation OU "GROUPS"
New-ADOrganizationalUnit -Name "GROUPS" -Path "$domainname"
# Creation des groupes globaux
Do {
    $Service = Read-Host "Entrer le nom du groupe à créer dans SERVICES" 
    $Question = Read-Host "Creer un autre groupes? O/N"
    New-ADGroup -Name G-$Service -DisplayName G-$Service -GroupCategory Security -GroupScope Global -Path "OU=GROUPS,OU=SERVICES,$domainname"
} while ($Question -eq "O")

Do {
    $Service = Read-Host "Entrer le nom du groupe à créer dans USINE" 
    $Question = Read-Host "Creer un autre groupes? O/N"
    New-ADGroup -Name G-$Service -DisplayName G-$Service -GroupCategory Security -GroupScope Global -Path "OU=GROUPS,OU=USINE,$domainname"
} while ($Question -eq "O")


