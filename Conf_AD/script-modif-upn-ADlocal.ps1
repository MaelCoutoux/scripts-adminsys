# gestion des log
$logpathe = read-host "Voulez vous sauvegarder vos log de ce script"
Write-Output $logpathe
$logfile="$logpathe-"+(get-date -UFormat "%H%M-%Y%m%d")+".txt"
Start-Transcript $logfile

#-------------------------------------------------------------

# import du module active directory
import-module activedirectory

# demander upn
$NewUpn = read-host "quel nouvel upn voulez-vous renseigner "
Write-Output "vous avez choisi : $NewUpn"

# demande de l'OU de filtrage pour votre entreprise
$OU_Entreprise=read-host "indiquez a partir de quelle OU vous voulez filtrer (format OU=...,DC=...,DC=local)"
Write-Output "vous avec choisi : $OU_Entreprise"

# recuperation des utilisateur dans l'AD
$users = get-ADuser -LdapFilter "(!description=*)" -SearchBase $OU_Entreprise | Select-Object userprincipalname
Write-Output "ces utilisateurs vont etre modifie $users"

foreach ($user in $users) {

# recuperation du prefixe
$Prefixe = $user.userprincipalname.Split("@")[0]
Write-Output $Prefixe

# creation du nouvel UPN
$UPN = "$($Prefixe)@"+$NewUpn
Write-Output $UPN

# modification de l'UPN
Set-ADUser -Identity $Prefixe -UserPrincipalName $UPN
}

#-------------------------------------------------------------
Stop-Transcript