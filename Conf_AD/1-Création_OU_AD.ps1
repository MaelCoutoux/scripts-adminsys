﻿
# Variables
$entreprise=(read-host -Prompt "Nom entreprise ?")
$Infosdomaine = Get-ADDomain
$domainname = $infosdomaine.DistinguishedName

#Création de l'OU sociéte à la racine du domaine
New-ADOrganizationalUnit -Name "$entreprise" -Path "$domainname"

# Creation des array avec les noms des differentes OU a créer
$OUlvl1 = "SERVICES", "USINE" 
$OUlvl2 = "USERS", "COMPUTERS", "SERVERS"

#Boucle Foreach pour Creer les OU
foreach ($i in $OUlvl1) {
    New-ADOrganizationalUnit -Name $i -path "OU=$entreprise,$domainname"
    foreach ($j in $OUlvl2) {
        New-ADOrganizationalUnit -Name $j -Path "OU=$i,OU=$entreprise,$domainname"
    }
}

# Creation des OU dans USERS/SERVICES
$OUlvl3 = "Comptabilite", "Commercial", "RH", "Informatique", "Direction"
foreach ($k in $OUlvl3) {
    New-ADOrganizationalUnit -Name $k -Path "OU=USERS,OU=SERVICES,OU=$entreprise,$domainname"
}

# Activation de la corbeille AD
$activateBin = Read-Host "Activation de la corbeille AD ? y/n "
if ($activateBin -eq "y") {
    Enable-ADOptionalFeature -Identity "CN=Recycle Bin Feature,CN=Optional Features,CN=Directory Service,CN=Windows NT,CN=Services,CN=Configuration,$domainename" -Scope ForestOrConfigurationSet -Target 'contoso.com'
}

