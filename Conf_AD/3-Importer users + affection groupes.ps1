﻿
# Variables qui importe le fichier csv et qui parametre les mots de passe
$csv = Import-Csv -Path "C:\Users\Administrateur\Downloads\users.csv" -Delimiter ";"
$Password = "P@ssw0rd"
$Password = ConvertTo-SecureString $Password –AsPlainText –Force


# Creation des users
foreach ($ligne in $csv) {
    New-ADUser -AccountPassword $Password -Department $ligne.Department -UserPrincipalName $ligne.SamAccountName -Surname $ligne.Nom 
    -displayName $ligne.Name -Name $ligne.Nom -GivenName $ligne.Prenom -SamAccountName $ligne.SamAccountName 
    -ChangePasswordAtLogon $true -PasswordNeverExpires $false -Enabled $true -path $ligne.Path
}

# Ajout des membres dans les groupes a partir du csv
foreach ($ligne in $csv) {
    Add-ADGroupMember -Identity $ligne.Groupes -Members $ligne.SamAccountName
}

# Affections supplementaires
Add-ADGroupMember -Identity G-Secretaires -Members guy
Add-ADGroupMember -Identity G-Stagiaires -Members firmin



# Ajout des users dans groupes selon un critère 
$User_Caen = (Get-ADUser -Filter * -Properties * | Where-Object city -eq "Caen").SamAccountName
$User_Tours = (Get-ADUser -Filter * -Properties * | Where-Object city -eq "Tours").SamAccountName
$User_Lille = (Get-ADUser -Filter * -Properties * | Where-Object city -eq "Lille").SamAccountName
$User_Nantes = (Get-ADUser -Filter * -Properties * | Where-Object city -eq "Nantes").SamAccountName

foreach ($users in $User_Caen) {
    Add-ADGroupMember -Identity GG_Caen -Members $users
}

foreach ($users in $User_Tours) {
    Add-ADGroupMember -Identity GG_Tours -Members $users
}

foreach ($users in $User_Lille) {
    Add-ADGroupMember -Identity GG_Lille -Members $users
}

foreach ($users in $User_Nantes) {
    Add-ADGroupMember -Identity GG_Nantes -Members $users
}