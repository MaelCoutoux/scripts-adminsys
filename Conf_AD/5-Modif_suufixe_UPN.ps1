
# Script pour modifier le suffixe UPN des utilisateurs AD

$LocalUsers = Get-ADUser -Filter "UserPrincipalName -like '*domaine.local'" -SearchBase "OU=Utilisateurs,OU=...,DC=domaine,DC=local" -Properties userPrincipalName -ResultSetSize $null

$LocalUsers | ForEach-Object {$newUpn = $_.UserPrincipalName.Replace("@domaine.local","@new_domaine.fr"); $_ | Set-ADUser -UserPrincipalName $newUpn}


